#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "$SCRIPT_DIR/.." || exit

docker run --rm -v "${PWD}/specification":/specification \
    -w /specification registry.gitlab.com/librefoodpantry/common-services/tools/swagger-cli:latest \
    swagger-cli validate openapi.yaml