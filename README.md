# InventorySystem API

## Overview

This is an OpenAPI specification of the InventorySystem API.
The entry point of the specification is in `src/index.yaml`
Versioned bundles are available in `bundles/`

## Status
- Under development

## Installation Instructions

### 1. Developer Guide

#### 1.1 Install development environment

To avoid dependency conflicts across projects, and to reduce variance
between development platforms, we use VS Code devcontainers.

New to VS Code devcontainers? Start here
https://code.visualstudio.com/docs/remote/containers 
and follow its installation instructions. Be sure to install and
configure Git too.

Now download, install, and run this project and its devcontainer as
follows.

1. Navigate to this project on GitLab and select
    `Clone -> Open in your IDE -> Visual Studio Code (HTTPS)`.
2. Select location to store the project.
3. Select "Reopen in container" when option is provided.


#### 1.2. Validate specification

```
npm run validate
```

**If you recieve the error** ```swagger-cli: command not found```**, run** ```npm -i swagger-cli```

#### 1.3. Bundle specification into a single file

```
npm run bundle outfile.yaml
```

For example, when creating a release 0.2.4 ...

```
npm run bundle bundles/inventory-api.0.2.4.yaml
```
## Usage Instructions
- To be determined

## Tools
- Swagger API
- To be determined


## License
[GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html)
