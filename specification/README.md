# InventorySystem API

The API specification is written in [OpenAPI](https://www.openapis.org/), which is based on 
[YAML](https://www.openapis.org/). This implementation, based off of an 
[infrastructure refactor](https://gitlab.com/groups/LibreFoodPantry/-/epics/14), the entire specification is
consolidated to one `openapi.yaml` file.

Files are included into another files [using `$ref:` entries](https://swagger.io/docs/specification/using-ref/).
[swagger-cli](https://www.npmjs.com/package/swagger-cli) is used to "bundle" these files into a single file which is
easier for clients to consume. [swagger-cli](https://www.npmjs.com/package/swagger-cli) is also used to validate the
specification.